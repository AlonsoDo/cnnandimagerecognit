﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvolutionalNeuralNetworksAndImageRecognit
{
    class Program
    {
        static void Main(string[] args)
        {
            int Total = 0;
            
            Console.WriteLine("Convolutional Neural Networks And Image Recognit");
            // Sample from https://www.youtube.com/watch?v=2-Ol7ZB0MmU

            // Create AlphaBet
            
            // simbol1 = "\"
            int[] simbol1 = { 1,-1,-1,
                             -1, 1,-1,
                             -1,-1, 1 };
            // simbol2 = "/"
            int[] simbol2 = {-1,-1, 1,
                             -1, 1,-1,
                              1,-1,-1 };
            // simbol3 = "X"
            int[] simbol3 = { 1,-1, 1,
                             -1, 1,-1,
                              1,-1, 1 };
            // simbol4 = "O"
            int[] simbol4 = {-1, 1,-1,
                              1,-1, 1,
                             -1, 1,-1 };

            // Test one simbol. You can choice any.
            int[] bufferSimbol = simbol1;
            
            // Convolution Layer            

            // Buffer For Diagonal '\'
            char[] diagonal1 = new char[4];
            
            // Create Filters

            // Filter 1 - Diagonal '\'            
            int[] filter1 = { 1,-1, 0,
                             -1, 1, 0,
                              0, 0, 0 };
            // Filter 2 - Diagonal '\'
            int[] filter2 = { 0, 1,-1,
                              0,-1, 1,
                              0, 0, 0 };
            // Filter 3 - Diagonal '\'
            int[] filter3 = { 0, 0, 0,
                              1,-1, 0,
                             -1, 1, 0 };
            // Filter 4 - Diagonal '\'
            int[] filter4 = { 0, 0, 0,
                              0, 1,-1,
                              0,-1, 1 };            

            // Make operations for Poolling Layer
            
            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter1[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal1[0] = '\\';
            }
            else
            {
                diagonal1[0] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter2[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal1[1] = '\\';
            }
            else
            {
                diagonal1[1] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter3[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal1[2] = '\\';
            }
            else
            {
                diagonal1[2] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter4[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal1[3] = '\\';
            }
            else
            {
                diagonal1[3] = ' ';
            }
            Total = 0;

            // Buffer For Diagonal '/'
            char[] diagonal2 = new char[4];

            // Create Filters

            // Filter 5 - Diagonal '/'            
            int[] filter5 = {-1, 1, 0,
                              1,-1, 0,
                              0, 0, 0 };
            // Filter 6 - Diagonal '/'
            int[] filter6 = { 0,-1, 1,
                              0, 1,-1,
                              0, 0, 0 };
            // Filter 7 - Diagonal '/'
            int[] filter7 = { 0, 0, 0,
                             -1, 1, 0,
                              1,-1, 0 };
            // Filter 8 - Diagonal '/'
            int[] filter8 = { 0, 0, 0,
                              0,-1, 1,
                              0, 1,-1 };

            // Make operations for Poolling Layer

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter5[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal2[0] = '/';
            }
            else
            {
                diagonal2[0] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter6[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal2[1] = '/';
            }
            else
            {
                diagonal2[1] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter7[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal2[2] = '/';
            }
            else
            {
                diagonal2[2] = ' ';
            }
            Total = 0;

            for (int i = 0; i < 9; i++)
            {
                Total += (bufferSimbol[i] * filter8[i]);
            }
            // Matching and save
            if (Total == 4)
            {
                diagonal2[3] = '/';
            }
            else
            {
                diagonal2[3] = ' ';
            }
            Total = 0;

            // Fully Connected Layer
            int[] item = new int[8];

            for (int i = 0; i < 4; i++)
            {
                // Check for '\'
                if (diagonal1[i] == '\\')
                {
                    item[i] = 1;
                }
                else
                {
                    item[i] = -1;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                // Check for '/'
                if (diagonal2[i] == '/')
                {
                    item[i+4] = 1;
                }
                else
                {
                    item[i+4] = -1;
                }
            }

            // Look for the simbol matching array item with the small filters
            
            // Simbol 'X'
            int[] simbolFilter1 = { 1,-1,-1, 1,
                                   -1, 1, 1,-1 };
            // The maximus and unique valor of the match is 8 so...Check
            for (int i = 0; i < 8; i++)
            {
                Total += (item[i] * simbolFilter1[i]);
            }
            if (Total == 8)
            {
                Console.WriteLine("The simbol is: " + "X");
            }
            Total = 0;

            // Simbol 'O'
            int[] simbolFilter2 = {-1, 1, 1,-1,
                                    1,-1,-1, 1 };
            // The maximus and unique valor of the match is 8 so...Check
            for (int i = 0; i < 8; i++)
            {
                Total += (item[i] * simbolFilter2[i]);
            }
            if (Total == 8)
            {
                Console.WriteLine("The simbol is: " + "O");
            }
            Total = 0;

            // Simbol '/'
            int[] simbolFilter3 = {-1,-1,-1,-1,
                                   -1, 1, 1,-1 };
            // The maximus and unique valor of the match is 8 so...Check
            for (int i = 0; i < 8; i++)
            {
                Total += (item[i] * simbolFilter3[i]);
            }
            if (Total == 8)
            {
                Console.WriteLine("The simbol is: " + "/");
            }
            Total = 0;

            // Simbol '\'
            int[] simbolFilter4 = { 1,-1,-1, 1,
                                   -1,-1,-1,-1 };
            // The maximus and unique valor of the match is 8 so...Check
            for (int i = 0; i < 8; i++)
            {
                Total += (item[i] * simbolFilter4[i]);
            }
            if (Total == 8)
            {
                Console.WriteLine("The simbol is: " + "\\");
            }
            
            Console.ReadKey();
        }
    }
}
